import time
from res import aws
import os


# Save the state of the resources into a file.
# The file will be used to check which resources
# should be created in the start of the program.
def save_state(bucket_state, queue_state, stack_state, role_state, function_state, policy_state):
    try:
        f = open('save_file.txt', 'w')
        f.write(f'{queue_state};{bucket_state};{stack_state};{role_state};{function_state};{policy_state}')
        f.close()
    except:
        print('ERROR: Saving process of resources failed!')
    return True

# Save the state of the resources into a file.
# The file will be used to check which resources
# should be created in the start of the program.
def save_config(role_config, bucket_policy_config, bucket_notification_config, lambda_config):
    try:
        f = open('save_config.txt', 'w')
        f.write(f'{role_config};{bucket_policy_config};{bucket_notification_config};{lambda_config}')
        f.close()
    except:
        print('ERROR: Saving process of configurations failed!')
    return True


# Main dialog of the program.
# Here we can set the state as 'quit'.
def main():
    print('\n> (0 = quit, 1 = upload files, 2 = show transcribed items, 3 = create resources, 4 = delete resources)')
    input_value = input('> Choose an action: ')
    if str(input_value).isnumeric() == True:
        input_value = int(input_value)
    # check the input and behave accordingly.
    if input_value == 0:
        return 'quit'
    elif input_value == 1:
            return 'upload'
    elif input_value == 2:
            return 'show'
    elif input_value == 3:
            return 'create'
    elif input_value == 4:
        return 'delete'
    else:
        print('\nInput value is not valid! Please try again...')
    return 'main'


# Confirmation dialog for creating a new set of AWS resources.
def create(can_create):
    input_value = ''
    # If all needed resources are created we cannot create any more off them.
    if can_create == True:
        print('\n>> (0 = cancel, 1 = confirm)')
        input_value = input(">> This action can cost money... " +
                                "Are you sure you want to create missing resources? ")
        if str(input_value).isnumeric() == True:
            input_value = int(input_value)
    else:
        print('\nAll needed resources are already created!')
        input_value = 0
    # Check the input and behave accordingly.
    if input_value == 0:
        return 'main'
    elif input_value == 1:
        return 'start'
    else:
        print('\nInput value is not valid! Please try again...')
    return 'create'


# Confirmation dialog for deleting a set of AWS resources.
def delete(can_delete):
    input_value = ''
    # If there are active resources we can delete them.
    if can_delete == True:
        print('\n>> (0 = cancel, 1 = confirm)')
        input_value = input(">> This action deletes some AWS resources " +
                                "saved in the save_file.txt depending of users choices. " +
                                "Are you sure you want to proceed? ")
        if str(input_value).isnumeric() == True:
            input_value = int(input_value)
    else:
        print('\nThere are no resources to delete!')
        input_value = 0
    # Check the input and behave accordingly.
    if input_value == 0:
        return 'main'
    elif input_value == 1:
        aws.delete_resources()
        return 'main'
    else:
        print('\nInput value is not valid! Please try again...')
    return 'delete'


# Confirmation dialog for uploading audio files into S3 bucket.
def upload(bucket_name, can_upload):
    input_value = ''
    # When all resources are created, we can upload files. Otherwise we cannot.
    if can_upload == True:
        print('\n>> (0 = cancel, 1 = confirm)')
        input_value = input(">> This action uploads all files " +
                                "from the './res/audio' folder into " +
                                "the S3 bucket in 30 second intervals. " +
                                "Do you really want to do that? ")
        if str(input_value).isnumeric() == True:
            input_value = int(input_value)
    else:
        print('\nSome resources are missing! Please, create them first...')
        input_value = 0
    # Check the input and behave accordingly.
    if input_value == 0:
        return 'main'
    elif input_value == 1:
        aws.upload_to_bucket('./res/audio', bucket_name)
        return 'main'
    else:
        print('\nInput value is not valid! Please try again...')
    return 'upload'


# Main program.
# Run the program until the state is set to 'quit'.
if __name__ == '__main__':
    # Resource names.
    queue_name = 'None'
    bucket_name = 'None'
    stack_name = 'None'
    role_name = 'None'
    function_name = 'None'
    policy_name = 'None'
    # Configuration names.
    role_config = 'None'
    bucket_policy_config = 'None'
    bucket_notification_config = 'None'
    lambda_config = 'None'
    print('Program is running...')

    state = 'start'
    # The value for 'state' is given by return values
    # of main(), create(), upload() and delete() functions.
    # The state defines what dialog appears for the user.
    while state != 'quit':
        if state == 'start':
            # Create resources.
            response = aws.create_resources()
            list_of_states = response[0].split(';')
            bucket_name = list_of_states[0]
            queue_name = list_of_states[1]
            stack_name = list_of_states[2]
            role_name = list_of_states[3]
            function_name = list_of_states[4]
            policy_name = list_of_states[5]
            list_of_configs = response[1].split(';')
            role_config = list_of_configs[0]
            bucket_policy_config = list_of_configs[1]
            bucket_notification_config = list_of_configs[2]
            lambda_config = list_of_configs[3]
            state = 'main'
        elif state == 'main':
            # Main dialog.
            state = main()
        elif state == 'upload':
            # Check if all resources are created. 
            # If not, the application could not run
            # and the upload process is not executed.
            is_all_created = True
            if (('None' in [queue_name, bucket_name, stack_name, role_name, function_name, policy_name]) or 
                ('None' in [role_config, bucket_policy_config, bucket_notification_config, lambda_config])):
                is_all_created = False
            # Uploading dialog.
            state = upload(bucket_name, is_all_created)
        elif state == 'show':
            # Show stored items in DynamoDB table.
            aws.show_transcribed_table(stack_name)
            state = 'main'
        elif state == 'create':
            # Check if all resources are created.
            # If true, the application does not need
            # any new resources.
            is_all_created = True
            if (('None' in [queue_name, bucket_name, stack_name, role_name, function_name, policy_name]) or 
                ('None' in [role_config, bucket_policy_config, bucket_notification_config, lambda_config])):
                is_all_created = False
            # Creation dialog.
            state = create(not is_all_created)
        elif state == 'delete':
            # Check if there are no resources to delete.
            is_nothing_created = False
            if all(value == 'None' for value in [queue_name, bucket_name, stack_name, role_name, function_name, policy_name]):
                is_nothing_created = True
            # Deletion dialog.
            state = delete(not is_nothing_created)
            # Get the new resource states.
            try:
                f = open('save_file.txt', 'r')
                content = f.read()
            except:
                print('ERROR: File called save_file.txt does not exist!')
            list_of_states = content.split(';')
            queue_name = list_of_states[0]
            bucket_name = list_of_states[1]
            stack_name = list_of_states[2]
            role_name = list_of_states[3]
            function_name = list_of_states[4]
            policy_name = list_of_states[5]
            # Get the new configuration states.
            try:
                f = open('save_config.txt', 'r')
                content = f.read()
            except:
                print('ERROR: File called save_config.txt does not exist!')
            list_of_states = content.split(';')
            role_config = list_of_states[0]
            bucket_policy_config = list_of_states[1]
            bucket_notification_config = list_of_states[2]
            lambda_config = list_of_states[3]
            
    print('\nProgram is closed...')