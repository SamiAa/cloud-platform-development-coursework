import logging
import boto3
from botocore.exceptions import ClientError
from zipfile import ZipFile
import os
from dotenv import load_dotenv


# Load environment variables from .env file.
load_dotenv()


# Create a new Lambda function.
def create_function(function_name, role_name, stack_name):
    function_name = function_name + 's2106793'
    role_name = role_name + 's2106793'
    stack_name = stack_name + 's2106793'

    try:
        # Create a .zip file from lambda_code.py and save_file.txt files. 
        # The .zip file will be initialized as Lambda functions code.
        with ZipFile('./res/lambda_code.zip', 'w') as zip:
            zip.write('./res/lambda_code.py', './lambda_code.py')
            # zip.write('./save_file.txt')

        # Read the .zip file and convert it to bytes.
        content = open('./res/lambda_code.zip', 'rb').read()

        # Create the Lambda function.
        lambda_client = boto3.client('lambda')
        lambda_client.create_function(
            FunctionName=function_name,
            Runtime='python3.8',
            Role=f"arn:aws:iam::{os.environ['AWS_ACCOUNT_NUM']}:role/{role_name}",
            Handler='lambda_code.lambda_handler',
            Timeout=30,
            Description='Invoke transcribe and comprehend ' +
                        'services and save the result in DynamoDB table.',
            Code={
                'ZipFile': content
            },
            PackageType='Zip',
            Environment={
                'Variables': {
                    'AWS_AREA': str(os.environ['AWS_AREA']),
                    'PHONE_NUM': str(os.environ['PHONE_NUM']),
                    'STACK_NAME': stack_name
                }
            }
        )
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Create a trigger for a Lambda function.
# Trigger is set to listen to an SQS queue.
def create_event_source_mapping(function_name, queue_name):
    queue_name = queue_name + 's2106793'
    function_name = function_name + 's2106793'

    try:
        lambda_client = boto3.client('lambda')
        lambda_client.create_event_source_mapping(
            EventSourceArn=f"arn:aws:sqs:{os.environ['AWS_AREA']}:{os.environ['AWS_ACCOUNT_NUM']}:{queue_name}",
            FunctionName=function_name,
            Enabled=True
        )
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Delete the trigger of a Lambda function.
def delete_event_source_mapping(function_name, queue_name):
    queue_name = queue_name + 's2106793'
    function_name = function_name + 's2106793'

    try:
        lambda_client = boto3.client('lambda')
        # Get event source mapping UUID.
        response = lambda_client.list_event_source_mappings(
            EventSourceArn=f"arn:aws:sqs:{os.environ['AWS_AREA']}:{os.environ['AWS_ACCOUNT_NUM']}:{queue_name}",
            FunctionName=function_name
        )

        # Delete event source mapping.
        if len(response) > 0:
            lambda_client.delete_event_source_mapping(
                UUID=response['EventSourceMappings'][0]['UUID']
            )
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Check if a function already exists.
def check_if_function_exists(function_name):
    function_name = function_name + 's2106793'
    exists = False

    try:
        lamda_client = boto3.client('lambda')
        response = lamda_client.list_functions()
        
        for function in response['Functions']:
            if function['FunctionName'] == function_name:
                exists = True
                break
    except ClientError as err:
        logging.error(err)
        return False
    return exists


# Delete a Lambda function.
def delete_function(function_name):
    function_name = str(function_name) + 's2106793'
    
    try:
        lambda_client = boto3.client('lambda')
        lambda_client.delete_function(FunctionName=function_name)
    except ClientError as err:
        logging.error(err)
        return False
    return True