import logging
import boto3
from botocore.exceptions import ClientError
import res.templates.sqs_policy as policy
import json
# import os


# Create a new SQS queue.
def create_queue(queue_name):
    queue_name = str(queue_name) + 's2106793'
    sqs_policy = json.dumps(policy.generate_sqs_policy(queue_name))

    try:
        sqs_client = boto3.client('sqs')
        sqs_client.create_queue(
            QueueName=queue_name,
            Attributes={
                'Policy': sqs_policy,
                # 'RedrivePolicy': "{" +
                #     "'deadLetterTargetArn': ''," + 
                #     "'maxReceiveCount': '5'" +
                # "}",
                'VisibilityTimeout': '180'
            }
        )
    except ClientError as err:
        logging.error(err)
        return False
    return True


# def send_message(queue_name):
#     queue_name = str(queue_name) + 's2106793'
#     try:
#         sqs_client = boto3.client('sqs')
#         queue_url = sqs_client.get_queue_url(QueueName=queue_name)['QueueUrl']
#         sqs_client.send_message(
#             QueueUrl=queue_url,
#             MessageBody='Bucket updated'
#         )
#     except ClientError as err:
#         logging.error(err)
#         return False
#     return True


# Delete an SQS queue.
def delete_queue(queue_name):
    queue_name = str(queue_name) + 's2106793'

    try:
        sqs_client = boto3.client('sqs')
        queue_url = sqs_client.get_queue_url(QueueName=queue_name)['QueueUrl']
        sqs_client.delete_queue(QueueUrl=queue_url)
    except ClientError as err:
        logging.error(err)
        return False
    return True

# Check if an SQS queue with a certain name exists.
def check_if_queue_exists(queue_name):
    queue_name = str(queue_name) + 's2106793'
    exists = False

    try:
        sqs_client = boto3.client('sqs')
        response = sqs_client.list_queues()
        keys = response.keys()
        key_exists = False
        
        # If no queues exist, list_queues() returns an object without
        # QueueUrls key for some reason. That's why the keys existance
        # is first checked here, to avoid an error.
        for key in keys:
            if key == 'QueueUrls':
                key_exists = True
        if key_exists == True:
            for queue in response['QueueUrls']:
                if str(queue).split('/')[-1] == queue_name:
                    exists = True
                    break
    except ClientError as err:
        logging.error(err)
        return False
    return exists