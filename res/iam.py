import logging
import boto3
from botocore.exceptions import ClientError
import res.templates.lambda_trust_policy as trust_policy
import res.templates.lambda_execution_policy as execution_policy
import json
import os
from dotenv import load_dotenv


# Load environment variables from .env file.
load_dotenv()


# Create a new Lambda execution role.
def create_lambda_execution_role(role_name):
    role_name = role_name + 's2106793'
    lambda_trust_policy = json.dumps(trust_policy.generate_lambda_trust_policy())

    try:
        iam_client = boto3.client('iam')
        iam_client.create_role(
            RoleName=role_name,
            AssumeRolePolicyDocument=lambda_trust_policy,
            Description='Execution role for Lambda function.'
        )
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Create a new Lambda execution policy.
def create_lambda_execution_policy(policy_name, queue_name, stack_name):
    policy_name = policy_name + 's2106793'
    queue_name = queue_name + 's2106793'
    stack_name = stack_name + 's2106793'
    lambda_execution_policy = json.dumps(execution_policy.generate_lambda_execution_policy(queue_name, stack_name))

    try:
        iam_client = boto3.client('iam')
        iam_client.create_policy(
            PolicyName=policy_name,
            PolicyDocument=lambda_execution_policy,
            Description='Execution policy for Lambda function.'
        )
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Attach the needed policies to the Lambda execution role.
def attach_role_policies(role_name, policy_name):
    role_name = role_name + 's2106793'
    policy_name = policy_name + 's2106793'

    try:
        iam_client = boto3.client('iam')
        iam_client.attach_role_policy(
            RoleName=role_name,
            PolicyArn=f"arn:aws:iam::{os.environ['AWS_ACCOUNT_NUM']}:policy/{policy_name}"
        )
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Detach policies from the Lambda execution role.
def detach_role_policies(role_name, policy_name):
    role_name = role_name + 's2106793'
    policy_name = policy_name + 's2106793'

    try:
        iam_client = boto3.client('iam')
        iam_client.detach_role_policy(
            RoleName=role_name,
            PolicyArn=f"arn:aws:iam::{os.environ['AWS_ACCOUNT_NUM']}:policy/{policy_name}"
        )
    except ClientError as err:
        logging.error(err)
        return False
    return True


# # Attach the needed policies to the Lambda execution role.
# def attach_role_policies(role_name):
#     role_name = role_name + 's2106793'

#     try:
#         iam_client = boto3.client('iam')
#         iam_client.attach_role_policy(
#             RoleName=role_name,
#             PolicyArn='arn:aws:iam::aws:policy/service-role/AWSLambdaSQSQueueExecutionRole'
#         )
#         iam_client.attach_role_policy(
#             RoleName=role_name,
#             PolicyArn='arn:aws:iam::aws:policy/AmazonTranscribeFullAccess'
#         )
#         iam_client.attach_role_policy(
#             RoleName=role_name,
#             PolicyArn='arn:aws:iam::aws:policy/ComprehendReadOnly'
#         )
#         iam_client.attach_role_policy(
#             RoleName=role_name,
#             PolicyArn='arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess'
#         )
#         iam_client.attach_role_policy(
#             RoleName=role_name,
#             PolicyArn='arn:aws:iam::aws:policy/AmazonSNSFullAccess'
#         )
#     except ClientError as err:
#         logging.error(err)
#         return False
#     return True


# # Detach the attached policies.
# def detach_role_policies(role_name):
#     role_name = role_name + 's2106793'

#     try:
#         iam_client = boto3.client('iam')
#         iam_client.detach_role_policy(
#             RoleName=role_name,
#             PolicyArn='arn:aws:iam::aws:policy/service-role/AWSLambdaSQSQueueExecutionRole'
#         )
#         iam_client.detach_role_policy(
#             RoleName=role_name,
#             PolicyArn='arn:aws:iam::aws:policy/AmazonTranscribeFullAccess'
#         )
#         iam_client.detach_role_policy(
#             RoleName=role_name,
#             PolicyArn='arn:aws:iam::aws:policy/ComprehendReadOnly'
#         )
#         iam_client.detach_role_policy(
#             RoleName=role_name,
#             PolicyArn='arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess'
#         )
#         iam_client.detach_role_policy(
#             RoleName=role_name,
#             PolicyArn='arn:aws:iam::aws:policy/AmazonSNSFullAccess'
#         )
#     except ClientError as err:
#         logging.error(err)
#         return False
#     return True


# Check if a certain IAM role exists.
def check_if_policy_exists(policy_name):
    policy_name = policy_name + 's2106793'
    exists = False

    try:
        iam_client = boto3.client('iam')
        response = iam_client.list_policies()

        for policy in response['Policies']:
            if policy['PolicyName'] == policy_name:
                exists = True
                break
    except ClientError as err:
        logging.error(err)
        return False
    return exists


# Check if a certain IAM role exists.
def check_if_role_exists(role_name):
    role_name = role_name + 's2106793'
    exists = False

    try:
        iam_client = boto3.client('iam')
        response = iam_client.list_roles()

        for role in response['Roles']:
            if role['RoleName'] == role_name:
                exists = True
                break
    except ClientError as err:
        logging.error(err)
        return False
    return exists


# Delete a Lamda execution policy.
def delete_lambda_execution_policy(policy_name):
    policy_name = str(policy_name) + 's2106793'

    try:
        iam_client = boto3.client('iam')
        iam_client.delete_policy(
            PolicyArn=f"arn:aws:iam::{os.environ['AWS_ACCOUNT_NUM']}:policy/{policy_name}"
        )
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Delete a Lamda execution role.
def delete_lambda_execution_role(role_name):
    role_name = str(role_name) + 's2106793'

    try:
        iam_client = boto3.client('iam')
        iam_client.delete_role(RoleName=role_name)
    except ClientError as err:
        logging.error(err)
        return False
    return True