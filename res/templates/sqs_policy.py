import os
from dotenv import load_dotenv


# Load environment variables from .env file.
load_dotenv()


# Policy for the SQS queue.
def generate_sqs_policy(queue_name):
    sqs_policy = {
        "Id": "Policy1616692388835",
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "Stmt1616692318088",
                "Action": [
                    "sqs:SendMessage"
                ],
                "Effect": "Allow",
                "Resource": f"arn:aws:sqs:{os.environ['AWS_AREA']}:{os.environ['AWS_ACCOUNT_NUM']}:{queue_name}",
                "Principal": {
                    "Service": [
                        "s3.amazonaws.com"
                    ]
                }
            },
            {
                "Sid": "Stmt1616692386238",
                "Action": [
                    "sqs:ReceiveMessage",
                    "sqs:DeleteMessage"
                ],
                "Effect": "Allow",
                "Resource": f"arn:aws:sqs:{os.environ['AWS_AREA']}:{os.environ['AWS_ACCOUNT_NUM']}:{queue_name}",
                "Principal": {
                    "Service": [
                        "lambda.amazonaws.com"
                    ]
                }
            }
        ]
    }

    return sqs_policy