import os
from dotenv import load_dotenv


# Load environment variables from .env file.
load_dotenv()


# Lambda function uses this policy.
def generate_lambda_execution_policy(queue_name, stack_name):
    table_name = stack_name + '-sentimentTables2106793'
    lambda_execution_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "Stmt1616930112930",
                "Action": [
                    "sqs:DeleteMessage",
                    "sqs:GetQueueAttributes",
                    "sqs:ReceiveMessage"
                ],
                "Effect": "Allow",
                "Resource": f"arn:aws:sqs:{os.environ['AWS_AREA']}:{os.environ['AWS_ACCOUNT_NUM']}:{queue_name}"
            },
            {
                "Sid": "Stmt1616930362106",
                "Action": [
                    "logs:CreateLogGroup",
                    "logs:CreateLogStream",
                    "logs:PutLogEvents"
                ],
                "Effect": "Allow",
                "Resource": "*"
            },
            {
                "Sid": "Stmt1616930549020",
                "Action": [
                    "transcribe:GetTranscriptionJob",
                    "transcribe:StartTranscriptionJob"
                ],
                "Effect": "Allow",
                "Resource": "*"
            },
            {
                "Sid": "Stmt1616930730168",
                "Action": [
                    "s3:GetObject"
                ],
                "Effect": "Allow",
                "Resource": "arn:aws:s3:::*transcribe*/*"
            },
            {
                "Sid": "Stmt1616930878458",
                "Action": [
                    "comprehend:DetectSentiment"
                ],
                "Effect": "Allow",
                "Resource": "*"
            },
            {
                "Sid": "Stmt1616931654648",
                "Action": [
                    "dynamodb:ListTables"
                ],
                "Effect": "Allow",
                "Resource": "*"
            },
            {
                "Sid": "Stmt1616931496498",
                "Action": [
                    "dynamodb:PutItem"
                ],
                "Effect": "Allow",
                "Resource": f"arn:aws:dynamodb:{os.environ['AWS_AREA']}:{os.environ['AWS_ACCOUNT_NUM']}:table/{table_name}*"
            },
            {
                "Sid": "Stmt1616931653306",
                "Action": [
                    "sns:Publish"
                ],
                "Effect": "Allow",
                "Resource": "*"
            }
        ]
    }

    return lambda_execution_policy