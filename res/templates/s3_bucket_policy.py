import os
from dotenv import load_dotenv


# Load environment variables from .env file.
load_dotenv()


# Policy for the S3 bucket.
def generate_bucket_policy(bucket_name, role_name):
    bucket_policy = {
        "Id": "Policy1616761429480",
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "Stmt1616761428034",
                "Action": [
                    "s3:GetObject"
                ],
                "Effect": "Allow",
                "Resource": f"arn:aws:s3:::{bucket_name}/*",
                "Principal": {
                    "AWS": [
                        f"arn:aws:iam::{os.environ['AWS_ACCOUNT_NUM']}:role/{role_name}"
                    ]
                }
            }
        ]
    }

    return bucket_policy