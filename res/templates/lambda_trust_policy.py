# Lambda trust policy for execution role.
def generate_lambda_trust_policy():
    lambda_trust_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "Service": "lambda.amazonaws.com"
                },
                "Action": [
                    "sts:AssumeRole"
                ]
            }
        ]
    }

    return lambda_trust_policy