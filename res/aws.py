import time
from app import save_state
from app import save_config
from res import s3
from res import sqs
from res import cloudformation
from res import iam
from res import lambda_service
from res import dynamodb
import os
from dotenv import load_dotenv


# Load environment variables from .env file.
load_dotenv()


# Create the needed resources and return the accepted resource names.
# To define which resources are missing, the save_file.txt will be used.
def create_resources():
    content = 'None;None;None;None;None;None'

    # Open the save_file.txt if it exists
    # and read its content.
    try:
        f = open('save_file.txt', 'r')
        content = f.read()
    except:
        print('ERROR: File called save_file.txt does not exist!')

    list_of_states = content.split(';')
    # Get the name of the resources that were saved 
    # in the save_file.txt. Those resources already existed.
    queue_name = list_of_states[0]
    bucket_name = list_of_states[1]
    stack_name = list_of_states[2]
    role_name = list_of_states[3]
    function_name = list_of_states[4]
    policy_name = list_of_states[5]

    # If any resources need to be created, show info text at the beginning.
    if 'None' in [queue_name, bucket_name, stack_name, role_name, function_name, policy_name]:
        print('\nCreating needed resources...')
        print('If you do not want to create a resource, leave the name of the resource empty...\n')

    # Create the needed resources below.
    # If the resource already exists, do nothing.

    # ----- Create an SQS queue. -----
    if queue_name == 'None':
        while True:
            queue_name = input('Please, give a name for an SQS queue: ')
            if queue_name == '':
                queue_name = 'None'
                break
            if sqs.check_if_queue_exists(queue_name) == False:
                if sqs.create_queue(queue_name) == True:
                    print('SUCCESS: SQS queue created!')
                    save_state(bucket_name, queue_name, stack_name, role_name, function_name, policy_name)
                    break
                else:
                    print('ERROR: SQS queue could not be created!')
            else:
                print(f"FAILED: SQS queue with name '{queue_name}' already exists!")
    # ----- Create a DynamoDB table with CloudFormation. -----
    if stack_name == 'None':
        while True:
            stack_name = input('Please, give a name for a CloudFormation stack: ')
            if stack_name == '':
                stack_name = 'None'
                break
            if cloudformation.check_if_stack_exists(stack_name) == False:
                if cloudformation.create_stack(stack_name) == True:
                    print('SUCCESS: CloudFormation stack created!')
                    save_state(bucket_name, queue_name, stack_name, role_name, function_name, policy_name)
                    break
                else:
                    print('ERROR: CloudFormation stack could not be created!')
            else:
                print(f"FAILED: CloudFormation stack with name '{stack_name}' already exists!")
    # ----- Create a Lambda execution policy. -----
    if policy_name == 'None':
        while True:
            if queue_name == 'None' or stack_name == 'None':
                print('Skipping creation of a Lambda execution policy, because either ' +
                    'SQS queue or DynamoDB table or both are missing! ' +
                    'These are used to define the policy...')
                break
            policy_name = input('Please, give a name for a Lambda execution policy: ')
            if policy_name == '':
                policy_name = 'None'
                break
            if iam.check_if_policy_exists(policy_name) == False:
                if iam.create_lambda_execution_policy(policy_name, queue_name, stack_name) == True:
                    print('SUCCESS: Lambda execution policy created!')
                    save_state(bucket_name, queue_name, stack_name, role_name, function_name, policy_name)
                    time.sleep(2) # Wait to ensure Lambda policy exists before attaching it to anything.
                    break
                else:
                    print('ERROR: Lambda execution policy could not be created!')
            else:
                print(f"FAILED: Lambda execution policy with name '{policy_name}' already exists!")
    # ----- Create a Lambda execution role and attach needed policies to it. -----
    if role_name == 'None':
        while True:
            if policy_name == 'None':
                print('Skipping creation of a Lambda execution role because the execution policy ' +
                    'which must be attached to the role does not exist...')
                break
            role_name = input('Please, give a name for a Lambda execution role: ')
            if role_name == '':
                role_name = 'None'
                break
            if iam.check_if_role_exists(role_name) == False:
                if iam.create_lambda_execution_role(role_name) == True:
                    print('SUCCESS: Lambda execution role created!')
                    save_state(bucket_name, queue_name, stack_name, role_name, function_name, policy_name)
                    time.sleep(2) # Wait to ensure Lambda role exists before attaching policies to it.
                    break
                else:
                    print('ERROR: Lambda execution role could not be created!')
            else:
                print(f"FAILED: Lambda execution role with name '{role_name}' already exists!")
    # ----- Create an S3 bucket and add a policy to it. -----
    if bucket_name == 'None':
        while True:
            bucket_name = input('Please, give a name for an S3 bucket: ')
            if bucket_name == '':
                bucket_name = 'None'
                break
            if s3.check_if_bucket_exists(bucket_name) == False:
                if s3.create_bucket(bucket_name, os.environ['AWS_AREA']) == True:
                    print('SUCCESS: S3 bucket created!')
                    save_state(bucket_name, queue_name, stack_name, role_name, function_name, policy_name)
                    time.sleep(2) # Wait to ensure bucket exists before putting policies to it.
                    break
                else:
                    print('ERROR: S3 bucket could not be created!')
            else:
                print(f"FAILED: S3 bucket with name '{bucket_name}' already exists!")
    # ----- Create a Lambda function and event fource mapping
    # which works as a trigger for the Lambda function. -----
    if function_name == 'None':
        while True:
            if role_name == 'None' or stack_name == 'None':
                print('Skipping creation of a Lambda function because the execution role ' +
                    'and/or CloudFormation stack which must be added to the function does not exist!')
                break
            function_name = input('Please, give a name for a Lambda function: ')
            if function_name == '':
                function_name = 'None'
                break
            if lambda_service.check_if_function_exists(function_name) == False:
                if lambda_service.create_function(function_name, role_name, stack_name) == True:
                    print('SUCCESS: Lambda function created!')
                    save_state(bucket_name, queue_name, stack_name, role_name, function_name, policy_name)
                    time.sleep(2) # Wait to ensure Lambda function exists before creating event source mapping.
                    break
                else:
                    print('ERROR: Lambda function could not be created!')
            else:
                print(f"FAILED: Lambda function with name '{function_name}' already exists!")

    # ----------------------------------------
    # Configure the resources below.
    # If the resources needed to create certain configurations
    # does not exist, those configurations are not created.

    content = 'None;None;None;None'
    # Open the save_config.txt and read its content.
    try:
        f = open('save_config.txt', 'r')
        content = f.read()
    except:
        print('ERROR: File called save_config.txt does not exist!')

    list_of_configs = content.split(';')
    role_config = list_of_configs[0]
    bucket_policy_config = list_of_configs[1]
    bucket_notification_config = list_of_configs[2]
    lambda_config = list_of_configs[3]

    # Configure the Lambda execution role.
    if role_config == 'None':
        if role_name != 'None' and policy_name != 'None':
            if iam.attach_role_policies(role_name, policy_name) == True:
                time.sleep(5) # Wait to ensure policies are added before doing anything else to avoid errors.
                print('Waiting for the Lambda execution policy to be attached to the execution role...')
                time.sleep(10) # Waiting some more...
                print('SUCCESS: Attached an execution policy to the Lambda execution role!')
                role_config = 'roleconfig'
                save_config(role_config, bucket_policy_config, bucket_notification_config, lambda_config)
            else:
                print('ERROR: Could not attach an execution policy to the Lambda execution role!')
    # Configure the S3 bucket.
    if bucket_policy_config == 'None':
        if role_name != 'None' and bucket_name != 'None':
            if s3.put_bucket_policy(bucket_name, role_name) == True:
                print('SUCCESS: S3 bucket policy added!')
                bucket_policy_config = 'bucketpolicyconfig'
                save_config(role_config, bucket_policy_config, bucket_notification_config, lambda_config)
            else:
                print('ERROR: S3 bucket policy could not be created!')
        # else:
        #     print('Cannot add a bucket policy because the Lambda role, the S3 bucket or neither, which ' +
        #         'are used to define the policy, does not exist!')
    if bucket_notification_config == 'None':
        if queue_name != 'None' and bucket_name != 'None':
            if s3.put_notification_configuration(bucket_name, queue_name) == True:
                print('SUCCESS: S3 bucket notification configuration added!')
                bucket_notification_config = 'bucketnotificationconfig'
                save_config(role_config, bucket_policy_config, bucket_notification_config, lambda_config)
            else:
                print('ERROR: S3 bucket notification configuration could not be added!')
        # else:
        #     print('Cannot add a notification configuration for the bucket ' +
        #         'because the SQS queue, S3 bucket or neither, which are used to define the configuration, does not exist!')
    # Configure the Lambda function.
    if lambda_config == 'None':
        if queue_name != 'None' and function_name != 'None':
            if lambda_service.create_event_source_mapping(function_name, queue_name) == True:
                print('SUCCESS: Lambda event source mapping added!')
                lambda_config = 'lambdaconfig'
                save_config(role_config, bucket_policy_config, bucket_notification_config, lambda_config)
            else:
                print('ERROR: Lambda event source mapping could not be added!')
        # else:
        #     print('Cannot create an event source mapping because the SQS queue, Lambda function or neither, ' +
        #         'which are used to define it, does not exist!')

    return [
        f'{bucket_name};{queue_name};{stack_name};{role_name};{function_name};{policy_name}',
        f'{role_config};{bucket_policy_config};{bucket_notification_config};{lambda_config}'
    ]


# Delete the existing AWS resources.
def delete_resources():
    content = 'None;None;None;None;None;None'

    # Open the save_file.txt if it exists
    # and read its content.
    try:
        f = open('save_file.txt', 'r')
        content = f.read()
    except:
        print('ERROR: File called save_file.txt does not exist!')

    list_of_states = content.split(';')
    # Get the name of the resources that were saved 
    # in the save_file.txt. Those resources exist.
    queue_name = list_of_states[0]
    bucket_name = list_of_states[1]
    stack_name = list_of_states[2]
    role_name = list_of_states[3]
    function_name = list_of_states[4]
    policy_name = list_of_states[5]
    # Flags for resources.
    delete_function = False
    delete_bucket = False
    delete_role = False
    delete_policy = False
    delete_stack = False
    delete_queue = False

    # If any resources exist, show info text at the beginning.
    if any(value != 'None' for value in [queue_name, bucket_name, stack_name, role_name, function_name, policy_name]):
        print('\nDeleting existing resources...')
        print("Give the name of the resource without 's2106793' postfix to delete it.")
        print('If you do not want to delete a resource, leave the input empty...\n')

    # Delete the existing resources below.
    # If the resource does not exist, do nothing.

    input_value = ''
    # Ask which resources to delete before deleting anything.
    # This is done to know which configurations should be deleted.
    if function_name != 'None':
        while True:
            input_value = input('Name of the Lambda function to delete: ')
            if input_value == '':
                break
            elif input_value != '' and input_value == function_name:
                delete_function = True
                break
            else:
                print('Lambda function with a given name does not exist!')
    if bucket_name != 'None':
        while True:
            input_value = input('Name of the S3 bucket to delete: ')
            if input_value == '':
                break
            elif input_value != '' and input_value == bucket_name:
                # print('Bucket name is needed in Lambda function code and thus Lambda function is also deleted...')
                # if function_name != 'None':
                #     delete_function = True
                delete_bucket = True
                break
            else:
                print('S3 bucket with a given name does not exist!')
    if role_name != 'None':
        while True:
            if delete_function == False and function_name != 'None':
                print('Cannot delete the Lambda execution role because the Lambda function still exists! Skipping...')
                break
            input_value = input('Name of the Lambda execution role to delete: ')
            if input_value == '':
                break
            elif input_value != '' and input_value == role_name:
                delete_role = True
                break
            else:
                print('Lambda execution role with a given name does not exist!')
    if policy_name != 'None':
        while True:
            if delete_role == False and role_name != 'None':
                print('Cannot delete the Lambda execution policy because the Lambda execution role still exists! Skipping...')
                break
            input_value = input('Name of the Lambda execution policy to delete: ')
            if input_value == '':
                break
            elif input_value != '' and input_value == policy_name:
                delete_policy = True
                break
            else:
                print('Lambda execution policy with a given name does not exist!')
    if stack_name != 'None':
        while True:
            input_value = input('Name of the CloudFormation stack to delete: ')
            if input_value == '':
                break
            elif input_value != '' and input_value == stack_name:
                if function_name != 'None':
                    if delete_function == False:
                        print('Deleting the stack makes the Lambda function useless because the sentiments ' +
                            'cannot be stored to anywhere and thus the Lambda function is also deleted...')
                    delete_function = True
                delete_stack = True
                break
            else:
                print('CloudFormation stack with a given name does not exist!')
    if queue_name != 'None':
        while True:
            input_value = input('Name of the SQS queue to delete: ')
            if input_value == '':
                break
            elif input_value != '' and input_value == queue_name:
                delete_queue = True
                break
            else:
                print('SQS queue with a given name does not exist!')

    # -------------------------------------------
    # Delete configurations.

    content = 'None;None;None;None'
    # Open the save_config.txt and read its content.
    try:
        f = open('save_config.txt', 'r')
        content = f.read()
    except:
        print('ERROR: File called save_config.txt does not exist!')

    list_of_configs = content.split(';')
    role_config = list_of_configs[0]
    bucket_policy_config = list_of_configs[1]
    bucket_notification_config = list_of_configs[2]
    lambda_config = list_of_configs[3]

    # Delete Lambda function configurations.
    if lambda_config != 'None':
        if delete_function == True or delete_queue == True:
            if lambda_service.delete_event_source_mapping(function_name, queue_name) == True:
                print('SUCCESS: Event source mapping deleted!')
                lambda_config = 'None'
                save_config(role_config, bucket_policy_config, bucket_notification_config, lambda_config)
            else:
                print('ERROR: Event source mapping could not be deleted!')
    # Delete S3 bucket configurations.
    if bucket_notification_config != 'None':
        if delete_bucket == True or delete_queue == True:
            if s3.put_notification_configuration(bucket_name, '') == True:
                print('SUCCESS: S3 bucket notification configuration deleted!')
                bucket_notification_config = 'None'
                save_config(role_config, bucket_policy_config, bucket_notification_config, lambda_config)
            else:
                print('ERROR: S3 bucket notification configuration could not be deleted!')
    if bucket_policy_config != 'None':
        if delete_bucket == True or delete_role == True:
            if s3.put_bucket_policy(bucket_name, role_name) == True:
                print('SUCCESS: S3 bucket policy deleted!')
                bucket_policy_config = 'None'
                save_config(role_config, bucket_policy_config, bucket_notification_config, lambda_config)
            else:
                print('ERROR: S3 bucket policy could not be deleted!')
    # Delete Lambda execution role configurations.
    if role_config != 'None':
        if delete_role == True:
            if iam.detach_role_policies(role_name, policy_name) == True:
                print('SUCCESS: Policies of the Lambda role detached!')
                role_config = 'None'
                save_config(role_config, bucket_policy_config, bucket_notification_config, lambda_config)
            else:
                print('ERROR: Policies of the Lambda role could not be detached!')

    # ----------------------------------------
    # Delete resources.

    # ----- Delete a Lambda function. -----
    if delete_function == True:
        if lambda_service.delete_function(function_name) == True:
            function_name = 'None'
            print('SUCCESS: Lambda function deleted!')
            save_state(bucket_name, queue_name, stack_name, role_name, function_name, policy_name)
        else:
            print('ERROR: Lambda function could not be deleted!')
    # ----- Delete an S3 bucket. Before the bucket could be
    # deleted, the files in it must be deleted. -----
    if delete_bucket == True:
        directory = './res/audio'
        list_of_objects = []
        for filename in os.listdir(directory):
            list_of_objects.append({
                'Key': filename
            })
            # break # Break should be removed
        if s3.delete_files(list_of_objects, bucket_name) == True:
            print('SUCCESS: Files in the S3 bucket deleted!')
        else:
            print('ERROR: Files in the S3 bucket could not be deleted!')
        if s3.delete_bucket(bucket_name) == True:
            bucket_name = 'None'
            print('SUCCESS: S3 bucket deleted!')
            save_state(bucket_name, queue_name, stack_name, role_name, function_name, policy_name)
        else:
            print('ERROR: S3 bucket could not be deleted!')
    # ----- Delete a Lambda execution role. Before the Lambda execution role
    # could be deleted, the policies attached to it must be detached. -----
    if delete_role == True:
        if iam.delete_lambda_execution_role(role_name) == True:
            role_name = 'None'
            print('SUCCESS: Lambda role deleted!')
            save_state(bucket_name, queue_name, stack_name, role_name, function_name, policy_name)
        else:
            print('ERROR: Lambda role could not be deleted!')
    # ----- Delete a Lambda execution policy. -----
    if delete_policy == True:
        if iam.delete_lambda_execution_policy(policy_name) == True:
            policy_name = 'None'
            print('SUCCESS: Lambda policy deleted!')
            save_state(bucket_name, queue_name, stack_name, role_name, function_name, policy_name)
        else:
            print('ERROR: Lambda policy could not be deleted!')
    # ----- Delete a CloudFormation stack. -----
    if delete_stack == True:
        if cloudformation.delete_stack(stack_name) == True:
            stack_name = 'None'
            print('SUCCESS: CloudFormation stack deleted!')
            save_state(bucket_name, queue_name, stack_name, role_name, function_name, policy_name)
        else:
            print('ERROR: CloudFormation stack could not be deleted!')
    # ----- Delete an SQS queue. -----
    if delete_queue == True:
        if sqs.delete_queue(queue_name) == True:
            queue_name = 'None'
            print('SUCCESS: SQS queue deleted!')
            save_state(bucket_name, queue_name, stack_name, role_name, function_name, policy_name)
        else:
            print('ERROR: SQS queue could not be deleted!')
        
    return [
        f'{bucket_name};{queue_name};{stack_name};{role_name};{function_name};{policy_name}',
        f'{role_config};{bucket_policy_config};{bucket_notification_config};{lambda_config}'
    ]


# Upload all files in directory into the S3 bucket in 30 second intervals.
def upload_to_bucket(directory, bucket_name):
    for filename in os.listdir(directory):
        file_path = os.path.join(directory, filename)
        if s3.upload_file(file_path, bucket_name) == True:
            print(f'SUCCESS: File {filename} uploaded into an S3 bucket!')
        else:
            print(f'ERROR: Could not upload {filename} file into an S3 bucket!')
        # break # Break should be removed
        time.sleep(30) # Wait for the Lambda function to run.
    return True


# Show all items in the DynamoDB table. Each item contains transcription results.
def show_transcribed_table(stack_name):
    if stack_name != 'None':
        response = dynamodb.query_all(stack_name)
        if len(response) > 0 and len(response['Items']) > 0:
            print(f"Items in the DynamoDB table '{response['Table']}':")

            # Find out how long is the the longest item name.
            # This is done to find out how many tabs should be added 
            # which helps printing items cleanly to the screen.
            max_length = 0
            for item in response['Items']:
                item_name_length = len(item['File']['S'])
                if max_length < item_name_length:
                    max_length = item_name_length

            # Calculate how many tabs are needed.
            tab_length = 8
            num_of_tabs = int(max_length / tab_length)

            # Print a header.
            extra_tabs = ''
            i = 0
            while i < num_of_tabs:
                extra_tabs += '\t'
                i += 1
            print(f'\tFile\t{extra_tabs}Positive\tNegative\tMixed')
            
            # Print items below.

            # Loop items list once more to print those items.
            for item in response['Items']:
                # Print an item.
                # Round numbers to create a clean output.
                col1 = item['File']['S']
                col2 = round(float(item['Positive']['N']), 10)
                col3 = round(float(item['Negative']['N']), 10)
                col4 = round(float(item['Mixed']['N']), 10)
                print(f"\t{col1}\t{col2}\t{col3}\t{col4}")
        else:
            print('Table is empty...')
    else:
        print("\nDynamoDB table is missing! CloudFormation stack must be created before showing the table's items...")
    return True