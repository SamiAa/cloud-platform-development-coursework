from __future__ import print_function
import time
import logging
import boto3
from botocore.exceptions import ClientError
import datetime
import json
import urllib
import os


# This function handles the trigger event of the Lambda function.
def lambda_handler(event, context):
    for record in event['Records']:
        print(json.dumps(record))
        payload = json.loads(record["body"])
        
        # Run this block if the event is not a test.
        if 'Records' in payload:
            bucket_name = payload['Records'][0]['s3']['bucket']['name']
            file_name = payload['Records'][0]['s3']['object']['key']
            media_format = str(file_name).split('.')[1]
            # Job name contains a hash number to make it unique.
            # This avoids a problem during transcribe process.
            job_name = (
                str(file_name).split('.')[0] + 
                str(
                    hash(
                        str(file_name).split('.')[0] + 
                        str(datetime.datetime.now()).replace(" ", "")
                    )
                ).replace("-", "")
            )
            job_uri = f"https://{bucket_name}.s3.{os.environ['AWS_AREA']}.amazonaws.com/{file_name}"

            # Transcribe the content of the audio file into text.
            transcription_text = transcribe(job_name, job_uri, media_format)

            # Create a sentiment analysis for the transcription text.
            sentiment_scores = []
            if transcription_text != '':
                sentiment_scores = comprehend(transcription_text)

            # Add the result to the DynamoDB table if result is available.
            if len(sentiment_scores) > 0:
                store(file_name, sentiment_scores)

                # Send an SMS if the sentiment is negative.
                if sentiment_scores[0] == 'NEGATIVE':
                    print('Negative sentiment detected! Send SMS message...')
                    # notify(file_name)


# Transcribe an audio file into text.
def transcribe(job_name, job_uri, media_format):
    transcription_text = ''

    try:
        # Start transcripting.
        transcribe_client = boto3.client('transcribe')
        transcribe_client.start_transcription_job(
            TranscriptionJobName=job_name,
            Media={'MediaFileUri': job_uri},
            MediaFormat=media_format,
            LanguageCode='en-US'
        )

        # Check when the transcription process ends.
        while True:
            status = transcribe_client.get_transcription_job(TranscriptionJobName=job_name)
            if status['TranscriptionJob']['TranscriptionJobStatus'] in ['COMPLETED', 'FAILED']:
                break
            time.sleep(5)

        print(status)

        # Download transcrption result and parse the needed data.
        transcription_uri = status['TranscriptionJob']['Transcript']['TranscriptFileUri']
        transcription_content = urllib.request.urlopen(transcription_uri).read().decode('UTF-8')
        data = json.loads(transcription_content)
        print(json.dumps(data))
        transcription_text = data['results']['transcripts'][0]['transcript']
        print(str(transcription_text))
    except ClientError as err:
        logging.error(err)
    return transcription_text


# Make a sentiment analysis for the transcription text.
def comprehend(text):
    sentiment_scores = []

    try:
        # Comprehend.
        comprehend_client = boto3.client('comprehend')
        response = comprehend_client.detect_sentiment(
            Text=text,
            LanguageCode='en'
        )

        print(json.dumps(response))

        # Parse the data.
        sentiment_scores.append(response['Sentiment'])
        sentiment_scores.append(response['SentimentScore']['Positive'])
        sentiment_scores.append(response['SentimentScore']['Negative'])
        sentiment_scores.append(response['SentimentScore']['Neutral'])
        sentiment_scores.append(response['SentimentScore']['Mixed'])
    except ClientError as err:
        logging.error(err)
    return sentiment_scores


# Store the sentiment scores into DynamoDB table.
def store(file_name, sentiment_scores):
    positive = sentiment_scores[1]
    negative = sentiment_scores[2]
    mixed = sentiment_scores[4]

    try:
        # Get the stacks name from save_file.txt which is 
        # located in the same .zip folder than this Lambda code file.
        # f = open('save_file.txt', 'r')
        # stack_name = f.read().split(';')[2] + 's2106793'
        stack_name = os.environ['STACK_NAME']

        # Send an item to a DynamoDB table after 
        # finding the correct table name.
        dynamodb_client = boto3.client('dynamodb')
        response = dynamodb_client.list_tables()

        for table in response['TableNames']:
            if f'{stack_name}-sentimentTables2106793' in table:
                dynamodb_client.put_item(
                    TableName=table,
                    Item={
                        'File': {'S': str(file_name)},
                        'Positive': {'N': str(positive)},
                        'Negative': {'N': str(negative)},
                        'Mixed': {'N': str(mixed)}
                    }
                )
                print(f'Item added to a DynamoDB table: {table}')
                break
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Send SMS message to notify of a negative sentiment.
def notify(added_file):
    try:
        sns_client = boto3.client('sns')
        sns_client.publish(
            PhoneNumber=f"{os.environ['PHONE_NUM']}",
            Message=f'File {added_file} got a negative sentiment!'
        )
    except ClientError as err:
        logging.error(err)
        return False
    return True