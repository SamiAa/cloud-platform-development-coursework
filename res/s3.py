import logging
import boto3
from botocore.exceptions import ClientError
import res.templates.s3_bucket_policy as policy
import json
import os
from dotenv import load_dotenv


# Load environment variables from .env file.
load_dotenv()


# Create a new S3 bucket.
# If region is not defined, use default region.
def create_bucket(bucket_name, region=None):
    bucket_name = str(bucket_name) + 's2106793'
    
    try:
        if region is None:
            s3_client = boto3.client('s3')
            s3_client.create_bucket(
                ACL='private',
                Bucket=bucket_name
            )
        else:
            s3_client = boto3.client('s3', region_name=region)
            location = {'LocationConstraint': region}
            s3_client.create_bucket(Bucket=bucket_name,
                                    CreateBucketConfiguration=location)
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Put a policy to an S3 bucket.
def put_bucket_policy(bucket_name, role_name):
    bucket_name = bucket_name + 's2106793'

    if role_name != '':
        role_name = role_name + 's2106793'
        bucket_policy = json.dumps(policy.generate_bucket_policy(bucket_name, role_name))

        try:
            s3_client = boto3.client('s3')
            s3_client.put_bucket_policy(Bucket=bucket_name,
                                        Policy=bucket_policy)
        except ClientError as err:
            logging.error(err)
            return False
    else:
        try:
            s3_client = boto3.client('s3')
            s3_client.put_bucket_policy(Bucket=bucket_name,
                                        Policy=json.dumps({}))
        except ClientError as err:
            logging.error(err)
            return False
    return True


# Configure an S3 bucket to send a message to SQS queue 
# when objects are created in the bucket.
def put_notification_configuration(bucket_name, queue_name):
    bucket_name = bucket_name + 's2106793'

    if queue_name != '':
        queue_name = queue_name + 's2106793'

        try:
            s3_client = boto3.client('s3')
            s3_client.put_bucket_notification_configuration(
                Bucket=bucket_name,
                NotificationConfiguration={
                    'QueueConfigurations': [
                        {
                            'QueueArn': f"arn:aws:sqs:{os.environ['AWS_AREA']}:{os.environ['AWS_ACCOUNT_NUM']}:{queue_name}",
                            'Events': [
                                's3:ObjectCreated:*',
                            ]
                        }
                    ]
                }
            )
        except ClientError as err:
            logging.error(err)
            return False
    else:
        try:
            s3_client = boto3.client('s3')
            s3_client.put_bucket_notification_configuration(
                Bucket=bucket_name,
                NotificationConfiguration={}
            )
        except ClientError as err:
            logging.error(err)
            return False
    return True


# # Create an access point for Transcibe service.
# def create_access_point(bucket_name):
#     bucket_name = bucket_name + 's2106793'
#     try:
#         s3_client = boto3.client('s3control')
#         s3_client.create_access_point(
#             AccountId=f"{os.environ['AWS_ACCOUNT_NUM']}",
#             Name='myaccesspoints2106793',
#             Bucket=bucket_name
#         )
#     except ClientError as err:
#         logging.error(err)
#         return False
#     return True


# Upload files into a bucket.
# New files are named the same in S3 than on a local computer.
def upload_file(file_path, bucket_name, object_name=None):
    bucket_name = str(bucket_name) + 's2106793'

    try:
        if object_name is None:
            object_name = str(file_path).split('\\')[-1]
        s3_client = boto3.client('s3')
        s3_client.upload_file(file_path, bucket_name, object_name)
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Delete objects from a S3 bucket.
def delete_files(list_of_objects, bucket_name):
    bucket_name = str(bucket_name) + 's2106793'

    try:
        s3_client = boto3.client('s3')
        s3_client.delete_objects(
            Bucket=bucket_name,
            Delete={
                'Objects': list_of_objects
            }
        )
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Delete a bucket.
def delete_bucket(bucket_name):
    bucket_name = str(bucket_name) + 's2106793'

    try:
        s3_client = boto3.client('s3')
        s3_client.delete_bucket(Bucket=bucket_name)
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Check if a certain S3 bucket exists.
def check_if_bucket_exists(bucket_name):
    bucket_name = str(bucket_name) + 's2106793'
    exists = False

    try:
        s3_client = boto3.client('s3')
        response = s3_client.list_buckets()
        
        for bucket in response['Buckets']:
            if bucket['Name'] == bucket_name:
                exists = True
                break
    except ClientError as err:
        logging.error(err)
        return False
    return exists