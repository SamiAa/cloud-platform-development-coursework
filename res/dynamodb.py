import logging
import boto3
from botocore.exceptions import ClientError
# from boto3.dynamodb.conditions import Key, Attr


# Get all items from a DynamoDB table which is created by a CloudFormation template.
def query_all(stack_name):
    stack_name = stack_name + 's2106793'

    try:
        dynamodb_client = boto3.client('dynamodb')
        response = dynamodb_client.list_tables()
        table_content = {}
        # Find the full table name first.
        for table in response['TableNames']:
            if f'{stack_name}-sentimentTables2106793' in table:
                # Then get the items.
                response2 = dynamodb_client.scan(
                    TableName=table,
                    Select='ALL_ATTRIBUTES'
                )
                table_content = {
                    'Table': table,
                    'Items': response2['Items']
                }
                break
    except ClientError as err:
        logging.error(err)
        return False
    return table_content