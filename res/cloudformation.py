import logging
import boto3
from botocore.exceptions import ClientError
import res.templates.cloudformation_template as template
import json


# Create a DynamoDB table with a CloudFormation template.
def create_stack(stack_name):
    stack_name = str(stack_name) + 's2106793'
    cloudformation_template = json.dumps(template.generate_cloudformation_template())

    try:
        cloudformation_client = boto3.client('cloudformation')
        cloudformation_client.create_stack(
            StackName=stack_name,
            TemplateBody=cloudformation_template,
            Parameters=[
                {
                    'ParameterKey': 'HashKeyElementName',
                    'ParameterValue': 'File'
                }
            ]
        )
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Delete a CloudFormation stack.
def delete_stack(stack_name):
    stack_name = str(stack_name) + 's2106793'

    try:
        # # Delete the DynamoDB table first to ensure deleting of stack.
        # dynamodb_client = boto3.client('dynamodb')
        # response = dynamodb_client.list_tables()
        # for table in response['TableNames']:
        # #     if f'{stack_name}-myDynamoDBTables2106793' in table:
        #     if f'{stack_name}-sentimentTables2106793' in table:
        #         dynamodb_client.delete_table(
        #             TableName=table
        #         )
        # Delete the stack.
        cloudformation_client = boto3.client('cloudformation')
        cloudformation_client.delete_stack(
            StackName=stack_name
        )
    except ClientError as err:
        logging.error(err)
        return False
    return True


# Check if a CloudFormation stack exists.
def check_if_stack_exists(stack_name):
    stack_name = str(stack_name) + 's2106793'
    exists = False

    try:
        cloudformation_client = boto3.client('cloudformation')
        response = cloudformation_client.list_stacks()
        
        for stack in response['StackSummaries']:
            if stack['StackName'] == stack_name:
                exists = True
                break
    except ClientError as err:
        logging.error(err)
        return False
    return exists