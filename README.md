# Cloud Platform Development - Coursework

## Introduction

The core part of this application is the boto3 Python library whose API documentation can be found here:
https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/index.html

This application creates the needed AWS resources at the start of this program. After that the user can choose to delete all AWS resources, create resources again or upload audio files from the './res/audio' folder into the S3 bucket created by this program.

After upload process is done this program sends a message to an SQS queue which acts as a trigger for a Lambda function. The Lambda function then gets the uploaded audio file and transcribes it to text format. Finally, a message is sent to a given phone number to inform the user that the process is completed.

## Files

File called app.py is the programs main file. AWS services are called in s3.py, sqs.py, cloudformation.py, iam.py and lamda_services.py files. File aws.py has a set of functions which stacks some of these AWS service files' functions with each other. Programs main file, app.py, calls AWS service functions through aws.py file. File save_file.txt has each of the needed AWS resources state, which is either 'None' if the resource does not exist or '<resource_name>' if the resource is created and active (not deleted). Templates folder has all used policies and other templates in .py format. Audio folder has the audio files which can be uploaded to the S3 bucket.

Requirements.txt file can be used to install the needed Python libraries on your environment.

## Installation

Follow the steps below to start with this application.

### Installation of AWS SDK

This application uses boto3 library which needs the AWS SDK installed and configured. Follow the instructions to install it in the link below:
https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html

### Create and activate Python virtual environment

Virtual environment is needed to avoid parallel versions of Python libraries. It is a good practise with Python. Create and activate a virtual environment by following the instructions in the link below:
https://docs.python.org/3/library/venv.html

### Install dependencies

Dependencies are saved in the requirements.txt file. They need to be installed on your virtual environment which should now be activated. Run the following terminal command in the root directory of this application:
```cmd
pip install -r requirements.txt
```

### Run the application

Now the application should be able to be ran by running the terminal command below in the root directory of this application:
```cmd
python3 app.py
```